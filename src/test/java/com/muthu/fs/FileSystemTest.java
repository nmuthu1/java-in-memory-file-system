package com.muthu.fs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.List;

import static com.muthu.fs.FSUtil.PATH_SEPARATOR;


/*
   @Author : Sam Muthu
   Date : April 11, 2020

   This file contains the Test cases for an in-memory file system. This implementation can be enhanced to
   include File system like windows and Linux and interfaces can be extracted and implemented. This is a
   Proof of concept project hence it may lack some limitations but has all the necessary test cases.

   Please refer the requirement.txt in the resources folder for the basic requirement of this simple POC.
 */

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FileSystemTest {

    private FileSystem fs;

    @BeforeAll
    void initAll() {
        fs = new FileSystem();
    }


    // Create drive and verify drive.
    @Test
    void createEntityDrive() {

        // For simplicity the drive name is first path element
        // Example: /ext/ or \ext\. windows style drive letter is not supported in this POC. (example: c:\folder1)
        String driveName = "ext";
        Drive expected = (Drive) fs.createEntity(new Drive(driveName));

        String path = expected.getPath();

        Assertions.assertEquals(PATH_SEPARATOR + "ext", path);

        Assertions.assertNotNull(expected);

        Drive actual = (Drive) fs.getFsEntity(PATH_SEPARATOR + driveName);

        Assertions.assertEquals(expected, actual);

        String driveName2 = "media";

        expected = (Drive) fs.createEntity(new Drive(driveName2));

        actual = (Drive) fs.getFsEntity(PATH_SEPARATOR + driveName2);
        Assertions.assertNotNull(actual);

        actual = fs.getDrive(driveName2);
        Assertions.assertEquals(expected, actual);
    }

    // Create drive and folder and verify created drive and folder.
    @Test
    void createFolder() {
        String folderName = "extFolder1";
        String driveName = "ext";

        // Drives are always created at the top level, no need to pass the path.
        Drive drive = (Drive) fs.createEntity(new Drive(driveName));
        String path = drive.getPath();

        Assertions.assertNotNull(drive);

        //  path : /ext
        String parentPath = PATH_SEPARATOR + driveName;
        Folder expected = (Folder) fs.createEntity(new Folder(folderName, parentPath));

        // folder path : /ext/extFolder1
        String folderPath = PATH_SEPARATOR + "ext" + PATH_SEPARATOR + folderName;
        Folder actual = (Folder) fs.getFsEntity(folderPath);

        Assertions.assertNotNull(actual);
        Assertions.assertEquals(expected, actual);
        Assertions.assertEquals("/ext/extFolder1", actual.getPath());

        // parent path : /ext
        Drive parent = (Drive) fs.getFsEntity(parentPath);
        Assertions.assertTrue(parent.getFiles().keySet().contains(folderName));
    }


    // Create drive, folder and file and verify created drive, folder, file, Size and Path.
    @Test
    void createFile() {
        String level1FolderName = "l1-d1";
        String drive1 = "ext";
        String fileName = "f1.txt";
        String fileContent = "Hello World!";
        String level2FolderName = "l2-d2";

        Drive drive = (Drive) fs.createEntity(new Drive(drive1));

        // folder1Path : /ext
        String folder1Path = PATH_SEPARATOR + drive1;
        Folder folder1 = (Folder) fs.createEntity(new Folder(level1FolderName, folder1Path));

        // folder2Path : /ext/l1-d1
        String folder2Path = PATH_SEPARATOR + drive1 + PATH_SEPARATOR + level1FolderName;
        Folder folder2 = (Folder) fs.createEntity(new Folder(level2FolderName, folder2Path));

        // filePath : /ext/l1-d1/l2-d2
        String filePath = PATH_SEPARATOR + drive1 + PATH_SEPARATOR + level1FolderName + PATH_SEPARATOR + level2FolderName;
        Folder actualLevel2FsEntity = (Folder) fs.getFsEntity(filePath);

        Assertions.assertEquals(filePath, actualLevel2FsEntity.getPath());

        int level2FsEntitySize = actualLevel2FsEntity.getSize();

        File file = (File) fs.createEntity(new File(fileName, filePath, fileContent));

        Assertions.assertTrue(actualLevel2FsEntity.getFiles().keySet().contains(fileName));

        File fileEntity = (File) actualLevel2FsEntity.getFiles().get(fileName);

        Assertions.assertEquals("/ext/l1-d1/l2-d2/f1.txt", fileEntity.getPath());

        Assertions.assertEquals(fileContent.getBytes().length, fileEntity.getSize());

        int fileSizeIncrement = fileContent.getBytes().length + fileName.getBytes().length;
        Assertions.assertEquals(actualLevel2FsEntity.getSize(), level2FsEntitySize + fileSizeIncrement);

    }

    // Create Zip file and verify.
    @Test
    void createZipFile() {
        String level1FolderName = "l1-d1";
        String drive1 = "ext";
        String file1Name = "f1.txt";
        String fileContent = "Hello World!";
        String level2FolderName = "l2-d2";

        Drive drive = (Drive) fs.createEntity(new Drive(drive1));

        // folder1Path : /ext
        String folder1Path = PATH_SEPARATOR + drive1;
        Folder folder1 = (Folder) fs.createEntity(new Folder(level1FolderName, folder1Path));

        // folder2Path : /ext/l1-d1
        String folder2Path = PATH_SEPARATOR + drive1 + PATH_SEPARATOR + level1FolderName;
        Folder folder2 = (Folder) fs.createEntity(new Folder(level2FolderName, folder2Path));

        // filePath : /ext/l1-d1/l2-d2
        String filePath = PATH_SEPARATOR + drive1 + PATH_SEPARATOR + level1FolderName + PATH_SEPARATOR + level2FolderName;
        Folder actualLevel2FsEntity = (Folder) fs.getFsEntity(filePath);

        Assertions.assertEquals(filePath, actualLevel2FsEntity.getPath());

        int level2FsEntitySize = actualLevel2FsEntity.getSize();

        File file1 = (File) fs.createEntity(new File(file1Name, filePath, fileContent));
        File file2 = (File) fs.createEntity(new File("l1f1.txt", folder2Path, fileContent));

        Assertions.assertNotNull(file1);
        Assertions.assertNotNull(file2);

        Assertions.assertTrue(actualLevel2FsEntity.getFiles().keySet().contains(file1Name));

        File file1Entity = (File) actualLevel2FsEntity.getFiles().get(file1Name);

        Assertions.assertEquals("/ext/l1-d1/l2-d2/f1.txt", file1Entity.getPath());

        Assertions.assertEquals(fileContent.getBytes().length, file1Entity.getSize());

        int fileSizeIncrement = fileContent.getBytes().length + file1Name.getBytes().length;
        Assertions.assertEquals(actualLevel2FsEntity.getSize(), level2FsEntitySize + fileSizeIncrement);


        // Create Zip file

        String zipFileName = "l1-d1.zip";

        Folder zippableFsEntity = (Folder) fs.getFsEntity(folder2Path);

        fs.createEntity(new ZipFile(zipFileName, folder2Path));

        ZipFile zippedFsEntity = (ZipFile) fs.getFsEntity(folder2Path + PATH_SEPARATOR + zipFileName);
        Assertions.assertEquals(zippableFsEntity.getSize(), zippedFsEntity.getParent().getSize());
        Assertions.assertTrue(zippableFsEntity.getFiles().containsKey(zipFileName));
    }


    // Write to file and verify the write.
    @Test
    void writeToFile() {
        String level1FolderName = "l1-d1";
        String drive1 = "ext";
        String fileName = "f1.txt";
        String fileContent = "Hello World!";
        String level2FolderName = "l2-d2";

        Drive drive = (Drive) fs.createEntity(new Drive(drive1));

        // folder1Path : /ext
        String folder1Path = PATH_SEPARATOR + drive1;
        Folder folder1 = (Folder) fs.createEntity(new Folder(level1FolderName, folder1Path));

        // folder2Path : /ext/l1-d1
        String folder2Path = PATH_SEPARATOR + drive1 + PATH_SEPARATOR + level1FolderName;
        Folder folder2 = (Folder) fs.createEntity(new Folder(level2FolderName, folder2Path));

        // filePath : /ext/l1-d1/l2-d2
        String filePath = PATH_SEPARATOR + drive1 + PATH_SEPARATOR + level1FolderName + PATH_SEPARATOR + level2FolderName;
        Folder actualLevel2FsEntity = (Folder) fs.getFsEntity(filePath);

        Assertions.assertEquals(filePath, actualLevel2FsEntity.getPath());

        int level2FsEntitySize = actualLevel2FsEntity.getSize();

        File file = (File) fs.createEntity(new File(fileName, filePath, fileContent));

        Assertions.assertNotNull(file);

        // Get the file just created.
        File fileFsEntity = (File) actualLevel2FsEntity.getFiles().get(fileName);

        Assertions.assertEquals(fileContent, fileFsEntity.getContent());

        // Append to existing file
        file = fs.createFile(new File(fileName, filePath, " - From Muthu!"));

        Assertions.assertEquals(fileContent + " - From Muthu!", fileFsEntity.getContent());
        Assertions.assertEquals(file.getContent(), fileFsEntity.getContent());
    }

    // Move file and folder and verify
    @Test
    void moveFsEntity() {
        String level1FolderName = "l1-d1";
        String drive1 = "ext";
        String fileName = "f1.txt";
        String fileContent = "Hello World!";
        String level2FolderName = "l2-d2";

        Drive drive = (Drive) fs.createEntity(new Drive(drive1));

        // folder1Path : /ext
        String folder1Path = PATH_SEPARATOR + drive1;
        Folder folder1 = (Folder) fs.createEntity(new Folder(level1FolderName, folder1Path));

        // folder2Path : /ext/l1-d1
        String folder2Path = PATH_SEPARATOR + drive1 + PATH_SEPARATOR + level1FolderName;
        Folder folder2 = (Folder) fs.createEntity(new Folder(level2FolderName, folder2Path));

        // filePath : /ext/l1-d1/l2-d2
        String filePath = PATH_SEPARATOR + drive1 + PATH_SEPARATOR + level1FolderName + PATH_SEPARATOR + level2FolderName;
        Folder actualLevel2FsEntity = (Folder) fs.getFsEntity(filePath);

        Assertions.assertEquals(filePath, actualLevel2FsEntity.getPath());

        int level2FsEntitySize = actualLevel2FsEntity.getSize();

        File file = (File) fs.createEntity(new File(fileName, filePath, fileContent));

        Assertions.assertNotNull(file);

        // Get the file just created.
        File fileFsEntity = (File) actualLevel2FsEntity.getFiles().get(fileName);

        Assertions.assertEquals(fileContent, fileFsEntity.getContent());

        // Append to existing file
        file = fs.createFile(new File(fileName, filePath, " - From Muthu!"));

        Assertions.assertEquals(fileContent + " - From Muthu!", fileFsEntity.getContent());

        // Move level2FolderName to      folder1Path
        // filePath : /ext/l1-d1/l2-d2, folder1Path : /ext
        // folder2Path : /ext/l1-d1
        // level2FolderName = "l2-d2"
        fs.moveFsEntity(filePath, folder1Path);

        Folder folder2FsEntity = (Folder) fs.getFsEntity(folder2Path);
        Drive folder1FsEntity = (Drive) fs.getFsEntity(folder1Path);

        Folder movedFsEntity = (Folder) folder1FsEntity.getFiles().get(level2FolderName);

        Assertions.assertNull(folder2FsEntity.getFiles().get(level2FolderName));
        Assertions.assertNotNull(folder1FsEntity.getFiles().get(level2FolderName));

        Assertions.assertEquals("/ext/l2-d2", movedFsEntity.getPath());

    }

    // Delete file and folder and verify
    @Test
    void deleteFsEntity() {
        String level1FolderName = "l1-d1";
        String drive1 = "ext";
        String fileName = "f1.txt";
        String fileContent = "Hello World!";
        String level2FolderName = "l2-d2";

        Drive drive = (Drive) fs.createEntity(new Drive(drive1));

        // folder1Path : /ext
        String folder1Path = PATH_SEPARATOR + drive1;
        Folder folder1 = (Folder) fs.createEntity(new Folder(level1FolderName, folder1Path));

        // folder2Path : /ext/l1-d1
        String folder2Path = PATH_SEPARATOR + drive1 + PATH_SEPARATOR + level1FolderName;
        Folder folder2 = (Folder) fs.createEntity(new Folder(level2FolderName, folder2Path));

        // filePath : /ext/l1-d1/l2-d2
        String filePath = PATH_SEPARATOR + drive1 + PATH_SEPARATOR + level1FolderName + PATH_SEPARATOR + level2FolderName;
        Folder actualLevel2FsEntity = (Folder) fs.getFsEntity(filePath);

        Assertions.assertEquals(filePath, actualLevel2FsEntity.getPath());

        int level2FsEntitySize = actualLevel2FsEntity.getSize();

        File file = (File) fs.createEntity(new File(fileName, filePath, fileContent));

        Assertions.assertNotNull(file);

        // Get the file just created.
        File fileFsEntity = (File) actualLevel2FsEntity.getFiles().get(fileName);

        Assertions.assertEquals(fileContent, fileFsEntity.getContent());

        // Append to existing file
        file = fs.createFile(new File(fileName, filePath, " - From Muthu!"));

        Assertions.assertEquals(fileContent + " - From Muthu!", fileFsEntity.getContent());

        int deletableFileSize = actualLevel2FsEntity.getSize();
        int preDeleteParentSize = actualLevel2FsEntity.getParent().getSize();
        // Delete level2FolderName
        // filePath : /ext/l1-d1/l2-d2
        // folder2Path : /ext/l1-d1
        // level2FolderName = "l2-d2"
        fs.deleteFsEntity(filePath);

        Folder folder2FsEntity = (Folder) fs.getFsEntity(folder2Path);

        Assertions.assertNull(folder2FsEntity.getFiles().get(level2FolderName));

        Assertions.assertEquals(preDeleteParentSize - deletableFileSize, folder2FsEntity.getSize());


    }

    // List directory - windows style
    @Test
    void dir() {
        String level1FolderName = "l1-d1";
        String drive1 = "ext";
        String fileName = "f1.txt";
        String fileContent = "Hello World!";
        String level2FolderName = "l2-d2";

        Drive drive = (Drive) fs.createEntity(new Drive(drive1));

        // folder1Path : /ext
        String folder1Path = PATH_SEPARATOR + drive1;
        Folder folder1 = (Folder) fs.createEntity(new Folder(level1FolderName, folder1Path));

        // folder2Path : /ext/l1-d1
        String folder2Path = PATH_SEPARATOR + drive1 + PATH_SEPARATOR + level1FolderName;
        Folder folder2 = (Folder) fs.createEntity(new Folder(level2FolderName, folder2Path));

        // filePath : /ext/l1-d1/l2-d2
        String filePath = PATH_SEPARATOR + drive1 + PATH_SEPARATOR + level1FolderName + PATH_SEPARATOR + level2FolderName;
        Folder actualLevel2FsEntity = (Folder) fs.getFsEntity(filePath);

        Assertions.assertEquals(filePath, actualLevel2FsEntity.getPath());

        int level2FsEntitySize = actualLevel2FsEntity.getSize();

        File file = (File) fs.createEntity(new File(fileName, filePath, fileContent));

        Assertions.assertNotNull(file);

        // Listing and verification

        List<String> filePathList = fs.dir(filePath);

        Assertions.assertEquals(List.of("f1.txt"), filePathList);

        List<String> folder2PathList = fs.dir(folder2Path);

        Assertions.assertEquals(List.of("l2-d2"), folder2PathList);

        List<String> folder1PathList = fs.dir(folder1Path);

        Assertions.assertEquals(List.of("l1-d1"), folder1PathList);

    }

    // listing directory linux style
    // @Test
    void ls() {
        this.dir();
    }
}