package com.muthu.fs;

public class FSUtil {
    public static final String PATH_SEPARATOR = "/";

    public static void populateSampleFileSystem(FileSystem fs, FileSystemEntity drive) {
        String level1FolderName = "l1-d1";
        String drive1 = drive.getName();
        String file1Name = "f1.txt";
        String fileContent = "Hello World!";
        String level2FolderName = "l2-d2";


        // folder1Path : /ext
        String folder1Path = PATH_SEPARATOR + drive1;
        Folder folder1 = (Folder) fs.createEntity(new Folder(level1FolderName, folder1Path));

        // folder2Path : /ext/l1-d1
        String folder2Path = PATH_SEPARATOR + drive1 + PATH_SEPARATOR + level1FolderName;
        Folder folder2 = (Folder) fs.createEntity(new Folder(level2FolderName, folder2Path));

        // filePath : /ext/l1-d1/l2-d2
        String filePath = PATH_SEPARATOR + drive1 + PATH_SEPARATOR + level1FolderName + PATH_SEPARATOR + level2FolderName;
        Folder actualLevel2FsEntity = (Folder) fs.getFsEntity(filePath);


        int level2FsEntitySize = actualLevel2FsEntity.getSize();

        File file1 = (File) fs.createEntity(new File(file1Name, filePath, fileContent));
        File file2 = (File) fs.createEntity(new File("l1f1.txt", folder2Path, fileContent));


        File file1Entity = (File) actualLevel2FsEntity.getFiles().get(file1Name);


        int fileSizeIncrement = fileContent.getBytes().length + file1Name.getBytes().length;

        // Create Zip file

        String zipFileName = "l1-d1.zip";

        Folder zippableFsEntity = (Folder) fs.getFsEntity(folder2Path);

        fs.createEntity(new ZipFile(zipFileName, folder2Path));


    }
}
