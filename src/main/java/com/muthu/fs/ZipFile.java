/*
 *
 *    @Author : Sam Muthu
 *    Date : 4/11/20, 10:25 AM
 */

package com.muthu.fs;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ZipFile extends Drive {

    public ZipFile(String name, String parentPath) {
        super(name);
        this.setEntityType(EntityType.ZIP);
        this.setParentPath(parentPath);
    }

}
