/*
 *
 *    @Author : Sam Muthu
 *    Date : 4/11/20, 10:27 AM
 *
 *
 *
 */


package com.muthu.fs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;


@NoArgsConstructor
public class Folder extends Drive {

    public Folder(String name, String parentPath) {
        super(name);
        this.setEntityType(EntityType.FOLDER);
        this.setParentPath(parentPath);
    }

    @Override
    @JsonIgnore
    public String getPath() {
        StringBuilder pathBuilder = new StringBuilder();

        //When files are moved the path depends on the current parent.
        if (getParent() != null) {
            pathBuilder.append(getParent().getPath());
        }
        pathBuilder.append(FSUtil.PATH_SEPARATOR);
        return pathBuilder.append(this.getName()).toString();
    }

}
