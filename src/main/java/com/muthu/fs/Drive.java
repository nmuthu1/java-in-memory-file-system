/*
 *
 *    @Author : Sam Muthu
 *    Date : May 11, 2020
 *
 */

package com.muthu.fs;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Drive extends FileSystemEntity {

    public Drive(String name) {
        super(name);
    }

}
