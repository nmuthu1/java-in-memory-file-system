/*
 *
 *    @Author : Sam Muthu
 *    Date : 4/11/20, 10:27 AM
 *
 */


package com.muthu.fs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class File extends Drive {


    public File(String name, String parentPath, String fileContent) {
        super(name);
        this.setParentPath(parentPath);
        this.setEntityType(EntityType.FILE);
        this.setContent(fileContent);
        this.setFiles(null);
    }

    @Override
    @JsonIgnore
    public String getPath() {
        StringBuilder pathBuilder = new StringBuilder();

        //When files are moved the path depends on the current parent.
        if (getParent() != null) {
            pathBuilder.append(getParent().getPath());
        }
        pathBuilder.append(FSUtil.PATH_SEPARATOR);
        return pathBuilder.append(this.getName()).toString();
    }

}
