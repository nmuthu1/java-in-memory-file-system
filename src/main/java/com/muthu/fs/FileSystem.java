
/*
   @Author : Sam Muthu
   Date : April 11, 2020

   This file depicts the implementation of in-memory file system. This implementation can be enhanced to
   include File system like windows and Linux and interfaces can be extracted and implemented. This is a
   Proof of concept project hence it may lack some limitations.

   Please go to the test folder to run all the tests and debug to see the complete working of this code.
 */

package com.muthu.fs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class FileSystem {


    HashMap<String, FileSystemEntity> fsDrives = new HashMap();
    FileSystemEntity currentDrive = null;
    ObjectMapper objectMapper = null;

    public FileSystem() {
        currentDrive = new Drive("default");
        fsDrives.put("default", currentDrive);
        objectMapper = new ObjectMapper();
        FSUtil.populateSampleFileSystem(this, currentDrive);
    }


    public List<String> dir(String path) {
        return ls(path);
    }

    public List<String> ls(String path) {

        FileSystemEntity fsEntity = currentDrive;
        if (path != null && !path.trim().equals("")) {
            fsEntity = this.getFsEntity(path);
        }

        if (fsEntity.getEntityType() == EntityType.FILE ||
                fsEntity.getEntityType() == EntityType.ZIP) {
            String[] paths = path.split(FSUtil.PATH_SEPARATOR);
            return List.of(paths[paths.length - 1]);
        }

        // return a sorted list of the target directory.
        return fsEntity.getFiles().keySet()
                .stream()
                .sorted()
                .collect(Collectors.toList());

    }

    public FileSystemEntity createEntity(FileSystemEntity entity) {

        FileSystemEntity result = null;
        switch (entity.getEntityType()) {
            case DRIVE: {
                result = createDrive((Drive) entity);
                break;
            }
            case FOLDER: {
                result = createFolder((Folder) entity);
                break;
            }
            case FILE: {
                result = createFile((File) entity);
                break;
            }
            case ZIP: {

                result = createZip((ZipFile) entity);
                break;

            }

        }
        return result;
    }

    private FileSystemEntity createDrive(Drive drive) {

        if (fsDrives.get(drive.getName()) == null) {
            fsDrives.put(drive.getName(), drive);
        }
        return fsDrives.get(drive.getName());
    }

    public Drive getDrive(String name) {
        return (Drive) this.fsDrives.get(name);
    }

    private Folder createFolder(Folder folder) {

        FileSystemEntity folderEntity = this.getFsEntity(folder.getParentPath());
        folder.setParent(folderEntity);
        folderEntity.getFiles().put(folder.getName(), folder);

        updateSizeRecursively(folderEntity, folder.getName().getBytes().length);
        return (Folder) folderEntity.getFiles().get(folder.getName());
    }


    private ZipFile createZip(ZipFile zipFile) {
        FileSystemEntity folderEntity = this.getFsEntity(zipFile.getParentPath());


        Folder toBeZipped = (Folder) folderEntity;
        int zippedSize = (toBeZipped.getSize() / 2) + zipFile.getName().getBytes().length;
        zipFile.setSize(zippedSize);

        String serializedZipContent = null;
        Folder zippedDeepCopy = null;
        try {
            serializedZipContent = objectMapper.writeValueAsString(folderEntity);
            zippedDeepCopy = objectMapper.readValue(serializedZipContent, Folder.class);
            System.out.println(zippedDeepCopy);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        zipFile.setFiles(zippedDeepCopy.getFiles());
        folderEntity.getFiles().put(zipFile.getName(), zipFile);
        setParentRecursively(folderEntity);
        this.updateSizeRecursively(folderEntity, zippedSize);
        return zipFile;
    }

    public File createFile(File file) {
        Folder currentDir = (Folder) getFsEntity(file.getParentPath());

        File currentFile = file;
        if (currentDir.getFiles().get(file.getName()) != null) {
            File existingFile = (File) currentDir.getFiles().get(file.getName());
            existingFile.setContent(existingFile.getContent() + file.getContent());
            currentFile = existingFile;
        } else {
            currentFile.setParent(currentDir);
            currentDir.getFiles().put(file.getName(), currentFile);
        }

        currentFile.setFiles(null);

        int fileSize = currentFile.getContent().getBytes().length;
        currentFile.setSize(fileSize);
        this.updateSizeRecursively(currentDir, fileSize + file.getName().getBytes().length);
        return currentFile;
    }

    /*
      TODO: remove this method since this is handled in the createFile method.
     */
    public void addContentToFile(File file, String path) {

        Folder currentDir = (Folder) getFsEntity(path);

        File currentFile = (File) currentDir.getFiles().get(file.getName());

        if (currentFile == null) {
            throw new IllegalArgumentException("Invalid path");
        }

        currentFile.setContent(currentFile.getContent() + file.getContent());
    }

    public String readContentFromFile(String name, String path) {
        Folder currentDir = (Folder) getFsEntity(path);

        File currentFile = (File) currentDir.getFiles().get(name);

        if (currentFile == null) {
            throw new IllegalArgumentException("Invalid path");
        }

        return currentFile.getContent();
    }

    public void deleteFsEntity(String path) {
        FileSystemEntity fsEntity = getFsEntity(path);

        String[] paths = path.split(FSUtil.PATH_SEPARATOR);

        if (fsEntity.getEntityType() == EntityType.DRIVE) {
            this.fsDrives.remove(fsEntity.getName());
        } else {
            FileSystemEntity parent = fsEntity.getParent();
            parent.getFiles().remove(fsEntity.getName());
            updateSizeRecursively(parent, (fsEntity.getSize() * -1));
        }
    }

    public void moveFsEntity(String srcPath, String destPath) {

        FileSystemEntity fsEntity = getFsEntity(srcPath);

        if (fsEntity.getEntityType() == EntityType.DRIVE) {
            throw new IllegalArgumentException("Drives can not be moved");
        }

        FileSystemEntity parent = fsEntity.getParent();
        FileSystemEntity moveableEntity = parent.getFiles().remove(fsEntity.getName());
        updateSizeRecursively(parent, (fsEntity.getSize() * -1));

        FileSystemEntity destFsEntity = getFsEntity(destPath);

        moveableEntity.setParent(destFsEntity);

        destFsEntity.getFiles().put(moveableEntity.getName(), moveableEntity);
        updateSizeRecursively(destFsEntity, moveableEntity.getSize());

    }

    public FileSystemEntity getFsEntity(String path) {

        String[] paths = path.split(FSUtil.PATH_SEPARATOR);

        if (paths.length < 2) {
            throw new IllegalArgumentException("Invalid path");
        }
        FileSystemEntity drive = this.getDrive(paths[1]);

        if (drive == null) {
            throw new IllegalArgumentException("Invalid path");
        }

        FileSystemEntity currentFsEntity = drive;

        for (int i = 2; i < paths.length; i++) {

            currentFsEntity = currentFsEntity.getFiles().get(paths[i]);

            if (currentFsEntity == null) {
                throw new IllegalArgumentException("Invalid path");
            }
        }
        return currentFsEntity;
    }

    private void updateSizeRecursively(FileSystemEntity fsEntity, int size) {

        FileSystemEntity currentLevel = fsEntity;

        while (currentLevel != null) {
            currentLevel.setSize(currentLevel.getSize() + size);
            ;
            currentLevel = currentLevel.getParent();
        }

    }

    private void setParentRecursively(FileSystemEntity fileSystemEntity) {

        if (fileSystemEntity.getFiles() == null || fileSystemEntity.getFiles().size() == 0)
            return;
        for (FileSystemEntity currentEntity : fileSystemEntity.getFiles().values()) {
            currentEntity.setParent(fileSystemEntity);
            setParentRecursively(currentEntity);
        }

    }

    /*
       TODO : This is to show how to implement the console but it is not implemented yet.
     */
    public static void main(String[] args) {

        FileSystem fs = new FileSystem();
        Scanner sc = new Scanner(System.in);

        String input;
        displayInitHelpMessages();
        while (true) {


            System.out.print(" $ ");

            input = sc.nextLine();  // Read user input
            System.out.println("Your input is: " + input);

            if (input.equalsIgnoreCase("exit")) {
                break;
            }
            processCommands(fs, input);
        }
    }

    /*
       TODO : This is to show how to implement the console but it is not implemented yet.
     */
    private static void processCommands(FileSystem fs, String input) {
        if (input.trim().startsWith("ls")) {

            List<String> result = fs.ls("");
            result.stream().forEach(System.out::println);
        }
    }

    /*
       TODO : This is to show how to implement the console but it is not implemented yet.
     */
    private static void displayInitHelpMessages() {
        System.out.println("\n\n-----------------------------------------------------\n");
        System.out.println(" Welcome to in-memory file system ");
        System.out.println(" You are on the default drive \n");
        System.out.println(" The following commands are supported ");
        System.out.println("\n Directory commands : ");
        System.out.println("  To display current working directory");
        System.out.println("    $ pwd\n");
        System.out.println("  To create a new directory");
        System.out.println("    $ mkdir <your_directory_name>\n");
        System.out.println("  To change to a directory");
        System.out.println("    $ cd <your_directory_name>\n");
        System.out.println("  To List the directory content");
        System.out.println("    $ cd <your_directory_name>\n");

        System.out.println("  To clear this console");
        System.out.println("    $ cls or clear");


        System.out.println("\n\n-----------------------------------------------------\n\n");
    }

}


