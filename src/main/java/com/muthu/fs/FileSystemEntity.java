/*
 *
 *    @Author : Sam Muthu
 *    Date : 4/12/20, 11:16 AM
 */

package com.muthu.fs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
public class FileSystemEntity {
    private String name;
    private EntityType entityType;
    private Map<String, FileSystemEntity> files = new HashMap<>();
    private int size = 0;
    private String parentPath;
    private String content = "";


    @JsonIgnore
    private FileSystemEntity parent = null;

    public FileSystemEntity(String name) {
        this.name = name;
        this.entityType = EntityType.DRIVE;
    }

    @JsonIgnore
    protected String getPath() {
        StringBuilder pathBuilder = new StringBuilder(FSUtil.PATH_SEPARATOR);
        return pathBuilder.append(name).toString();
    }


}
